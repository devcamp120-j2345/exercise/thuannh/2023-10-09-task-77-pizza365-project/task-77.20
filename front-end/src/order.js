$(document).ready(function () {
    var vTable;

    $('#btn-add-new-order').click(function () {
        $('#order-create-modal').modal('show');
    });

    $('#btn-create-order').click(function () {
        var vNewOrder = {
            orderDate: $('#inp-create-order-date').val(),
            requiredDate: $('#inp-create-required-date').val(),
            shippedDate: $('#inp-create-shipped-date').val(),
            status: $('#inp-create-status').val(),
            comments: $('#inp-create-comments').val(),
            customer: {
                id: $('#select-create-customer').val()
            }
        };

        $.ajax({
            type: "POST",
            url: "http://localhost:8080/orders",
            data: JSON.stringify(vNewOrder),
            contentType: "application/json",
            success: function (data) {
                vTable.row.add(data).draw(false);
                $('#order-create-modal').modal('hide');
                $('#createOrderForm')[0].reset();
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(xhr.responseText);
            }
        });
    });

    $(document).on('click', '.btn-edit', function () {
        var vOrderId = $(this).data('id');
        $('#order-update-modal').data('orderId', vOrderId);
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/orders/" + vOrderId,
            success: function (data) {
                $('#inp-update-order-date').val(data.orderDate);
                $('#inp-update-required-date').val(data.requiredDate);
                $('#inp-update-shipped-date').val(data.shippedDate);
                $('#inp-update-status').val(data.status);
                $('#inp-update-comments').val(data.comments);
                if (data.customer && data.customer.id) {
                    $('#select-update-customer').val(data.customer.id);
                } else {
                    $('#select-update-customer').val($('#select-update-customer option:first').val());
                }
                $('#order-update-modal').modal('show');
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(xhr.responseText);
            }
        });
    });

    $('#btn-update-order').click(function () {
        var vOrderId = $('#order-update-modal').data('orderId');

        var vUpdatedOrder = {
            orderDate: $('#inp-update-order-date').val(),
            requiredDate: $('#inp-update-required-date').val(),
            shippedDate: $('#inp-update-shipped-date').val(),
            status: $('#inp-update-status').val(),
            comments: $('#inp-update-comments').val(),
            customer: {
                id: $('#select-update-customer').val()
            }
        };

        $.ajax({
            type: "PUT",
            url: "http://localhost:8080/orders/" + vOrderId,
            data: JSON.stringify(vUpdatedOrder),
            contentType: "application/json",
            success: function (data) {
                var rowIndex = vTable.row('.selected').index();
                vTable.row(rowIndex).data(data).draw(false);
                $('#order-update-modal').modal('hide');
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(xhr.responseText);
            }
        });
    });

    $(document).on('click', '.btn-delete', function () {
        var vOrderId = $(this).data('id');
        $('#order-delete-modal').modal('show');
        $('#order-delete-modal').data('orderId', vOrderId);
    });

    $('#btn-delete-order').click(function () {
        var vOrderId = $('#order-delete-modal').data('orderId');

        $.ajax({
            type: "DELETE",
            url: "http://localhost:8080/orders/" + vOrderId,
            success: function () {
                vTable.row('.selected').remove().draw(false);
                $('#order-delete-modal').modal('hide');
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(xhr.responseText);
            }
        });
    });

    loadCustomersAndInitializeTable();

    function loadCustomersAndInitializeTable() {
        $.ajax({
            type: "GET",
            url: "http://localhost:8080/customers",
            success: function (data) {
                console.log(data);
                var options = '<option value="all">All</option>';

                for (var i = 0; i < data.length; i++) {
                    options += '<option value="' + data[i].id + '">' + data[i].firstName + ' ' + data[i].lastName + '</option>';
                }

                $('#select-create-customer').html(options);
                $('#select-update-customer').html(options);

                initializeDataTable();
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(xhr.responseText);
            }
        });
    }

    function initializeDataTable() {
        vTable = $('#order-table').DataTable({
            "ajax": {
                "url": "http://localhost:8080/orders",
                "dataSrc": ""
            },
            "columns": [
                {
                    "data": null,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    }
                },
                { "data": "orderDate" },
                { "data": "requiredDate" },
                { "data": "shippedDate" },
                { "data": "status" },
                { "data": "comments" },
                {
                    "data": null,
                    "render": function (data, type, row) {
                        return '<button class="btn btn-primary btn-edit" data-id="' + row.id + '">' +
                            '<i class="fas fa-edit"></i>' +
                            '</button>' +
                            '<button class="btn btn-danger btn-delete" data-id="' + row.id + '">' +
                            '<i class="fas fa-trash-alt"></i>' +
                            '</button>';
                    }
                }
            ]
        }).on('xhr', function (e, settings, json) {
            console.log(json);
        });
    }
});
